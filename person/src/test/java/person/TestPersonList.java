package person;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import org.junit.Test;

public class TestPersonList {

	@Test
	public void testCreateEmptyPersonList() {
		PersonList pL = new PersonList();
		assertThat(pL.getSize(), is(0));
	}
	
	@Test
	public void testAddOnePersonToList() {
		PersonList pL = new PersonList();
		pL.addPerson(new Person(1,"Fiachra"));
		Person[] persons = pL.getPersons();
		assertThat(persons.length, is(1));
		assertThat(persons[0].getName(), is("Fiachra"));
	}
	
	@Test
	public void  testAddTwoPersonsToList() {
		PersonList pL = new PersonList();
		pL.addPerson(new Person(1,"Fiachra"));
		pL.addPerson(new Person(2,"Martin"));
		assertThat(pL.getSize(), is(2));
	}
	
	@Test
	public void  testRemovePersonFromList() {
		PersonList pL = new PersonList();
		Person person1 = new Person(8,"Fiachra");
		Person person2 = new Person(9,"Martin");
		Person person3 = new Person(5,"Adam");
		Person person4 = new Person(1,"Mick");
		Person person5 = new Person(7,"Newton");
		pL.addPerson(person1);
		pL.addPerson(person2);
		pL.addPerson(person3);
		pL.addPerson(person4);
		pL.addPerson(person5);
		Person person = pL.removePerson(new Person(5, "Adam"));
		assertThat(pL.getSize(), is(4));
		assertThat(person.getName(), is("Adam"));
	}
	
	@Test
	public void testSortListByName() {
		PersonList pL = new PersonList();
		Person person1 = new Person(8,"Fiachra");
		Person person2 = new Person(9,"Martin");
		Person person3 = new Person(5,"Adam");
		Person person4 = new Person(1,"Mick");
		Person person5 = new Person(7,"Newton");
		pL.addPerson(person1);
		pL.addPerson(person2);
		pL.addPerson(person3);
		pL.addPerson(person4);
		pL.addPerson(person5);
		pL.sortListByName();
		assertThat(pL.getPersons()[0].getName(), is("Adam"));
		assertThat(pL.getPersons()[pL.getPersons().length-1].getName(), is("Newton"));
	}
	
	@Test
	public void testSortListByID() {
		PersonList pL = new PersonList();
		Person person1 = new Person(8,"Fiachra");
		Person person2 = new Person(9,"Martin");
		Person person3 = new Person(5,"Adam");
		Person person4 = new Person(1,"Mick");
		Person person5 = new Person(7,"Newton");
		pL.addPerson(person1);
		pL.addPerson(person2);
		pL.addPerson(person3);
		pL.addPerson(person4);
		pL.addPerson(person5);
		pL.sortListByID();
		assertThat(pL.getPersons()[0].getiD(), is(1));
		assertThat(pL.getPersons()[pL.getPersons().length-1].getiD(), is(9));
	}
	
	@Test
	public void testFindPersonWithLongestName() {
		PersonList pL = new PersonList();
		Person person1 = new Person(8,"Fiachra");
		Person person2 = new Person(9,"Martin");
		Person person3 = new Person(5,"Adam");
		Person person4 = new Person(1,"Mick");
		Person person5 = new Person(7,"Newton");
		pL.addPerson(person1);
		pL.addPerson(person2);
		pL.addPerson(person3);
		pL.addPerson(person4);
		pL.addPerson(person5);
		assertThat(pL.longestName(), is("Fiachra"));
	}

}
