package person;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import org.junit.Test;

public class testPerson {

	@Test
	public void testCreatePerson() {
		Person p = new Person(1, "Fiachra");
		assertNotNull(p);
		assertEquals(1, p.getiD());
		assertEquals("Fiachra", p.getName());
		assertThat(p.toString().indexOf("Fiachra"), is(not(-1)));
	}

}